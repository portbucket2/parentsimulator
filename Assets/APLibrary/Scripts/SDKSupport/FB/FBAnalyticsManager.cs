﻿using UnityEngine;
using System.Collections.Generic;
using Facebook.Unity;

namespace com.alphapotato.utility
{
    public class FBAnalyticsManager : MonoBehaviour
    {
        private bool IsFBActivated()
        {
            if (!FB.IsInitialized)
            {
                SDKSupport.Instance.fbManager.Initialize();
            }
            return FB.IsInitialized;
        }

        #region Public Callback

        public void FBALevelStart(int t_CurrentLevel)
        {
            if (IsFBActivated())
            {

                var t_FBEventParameter = new Dictionary<string, object>();
                t_FBEventParameter["Level_Info"] = "" + t_CurrentLevel;
                FB.LogAppEvent(
                    "Level_Start",
                    null,
                    t_FBEventParameter
                );
            }

        }

        public void FBALevelComplete(int t_CurrentLevel)
        {
            if (IsFBActivated())
            {

                var t_FBEventParameter = new Dictionary<string, object>();
                t_FBEventParameter["Level_Info"] = "" + t_CurrentLevel;
                FB.LogAppEvent(
                    "Level_Achieved",
                    null,
                    t_FBEventParameter
                );
            }

        }

        public void FBALevelFailed(int t_CurrentLevel)
        {
            if (IsFBActivated())
            {

                var t_FBEventParameter = new Dictionary<string, object>();
                t_FBEventParameter["Level_Info"] = "" + t_CurrentLevel;
                FB.LogAppEvent(
                    "Level_Failed",
                    null,
                    t_FBEventParameter
                );
            }

        }

        public void FBALevelScore(int levelIndex, string t_CurrentLevelScore)
        {
            if (IsFBActivated())
            {

                var t_FBEventParameter = new Dictionary<string, object>();
                t_FBEventParameter["Level_Info"] = "" + levelIndex + "-" + t_CurrentLevelScore;
                FB.LogAppEvent(
                    "Level_Score",
                    null,
                    t_FBEventParameter
                );
            }

        }

        public void FBPowerupCollected(string t_powerups)
        {
            if (IsFBActivated())
            {

                var t_FBEventParameter = new Dictionary<string, object>();
                t_FBEventParameter["Level_Info"] = "" + t_powerups;
                FB.LogAppEvent(
                    "Powerups",
                    null,
                    t_FBEventParameter
                );
            }
        }

        public void FBRewardedVideoAd(string t_AdType)
        {
            if (IsFBActivated())
            {

                FB.LogAppEvent(
                    "RewardedVideoAd_" + t_AdType,
                    1,
                    null
                );
            }
        }

        #endregion
    }
}