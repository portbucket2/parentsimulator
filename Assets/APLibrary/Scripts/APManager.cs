﻿using System;
using UnityEngine;
using com.alphapotato.utility;

[DefaultExecutionOrder(ConstantManager.APManagerOrder)]
public class APManager : MonoBehaviour
{
    public static APManager Instance;

    public ParticleSystem gameOverEffect;
    public static event Action OnGameStart;
    public static event Action OnGameReset;
    public static event Action OnGamePause;
    public static event Action<GameplayData> OnGameOver;
    public static event Action<Powerups> OnPowerupsTaken;
    public static event Action<GameState> OnGamestateChange;

    public int totalTaskToComplete = 0, currentCompletedTask = 0;
    public GameplayData gameplayData = new GameplayData();
    public GameState gameState;

    [HideInInspector]
    public APTools APTools;
    [HideInInspector]
    public SDKSupport SDKSupport;

    public virtual void Awake()
    {
        Instance = this;
        APTools = APTools.Instance;
        SDKSupport = SDKSupport.Instance;

    }

    public virtual void GameStart()
    {
        OnGamestateChange(GameState.PLAYING);
        SDKSupport.fbAnalyticsManager.FBALevelStart(APTools.sceneManager.GetLevelIndex() + 1);
    }

    public virtual void GameOver()
    {
        if (OnGameOver != null)
            OnGameOver(gameplayData);

        if (gameOverEffect != null && gameplayData.isGameoverSuccess)
        {
            gameOverEffect.Play();
        }
        GamestateChange(GameState.GAMEOVER);

        SDKSupport.fbAnalyticsManager.FBALevelComplete(APTools.sceneManager.GetLevelIndex() + 1);
    }

    public virtual void GamestateChange(GameState gameState)
    {
        this.gameState = gameState;
        if(OnGamestateChange != null)
            OnGamestateChange(gameState);
    }

    public virtual void OnCompleteATask()
    {
        currentCompletedTask++;

        if (currentCompletedTask.Equals(totalTaskToComplete))
        {
            gameplayData.isGameoverSuccess = true;
            gameplayData.gameStopTime = Time.time;

            GameOver();
        }
    }

    public virtual void NextLevel()
    {

        APTools.sceneManager.LoadNextLevel();
    }

}
