﻿using UnityEngine;

public class ConstantManager : MonoBehaviour
{
    public const string SOUND = "SOUND";
    public const string MUSIC = "MUSIC";
    public const string CURRENT_SCORE = "CURRENT_SCORE";
    public const string HIGH_SCORE = "HIGH_SCORE";

    // RuntimeExecution Order of APLibray Scripts.
    public const int SDKSupportOrder = -21;
    public const int APToolOrder = -20;
    public const int APBehaviourOrder = -3;
    public const int APManagerOrder = -2;

    // Collision Layer ids. 
    // These layer's order must be maintained in Layer Array to work properly with Collision
    public const int PLAYER_LAYER = 8;
    public const int ENEMY_LAYER = 9;
    public const int GROUND_LAYER = 10;
    public const int BOUNDARY_LAYER = 11;
    public const int PICKUPS_LAYER = 12;
    public const int DESTINATION_LAYER = 13;

}
