﻿using UnityEngine;
using com.alphapotato.utility;

[DefaultExecutionOrder(ConstantManager.APBehaviourOrder)]
public class APBehaviour : MonoBehaviour
{
    [HideInInspector]
    public APTools APTools;

    [HideInInspector]
    public Vector3 initialPosition, initialRotation, initialScale;

    [HideInInspector]
    public GameState gameState = GameState.NONE;

    public virtual void Awake()
    {
        APTools = APTools.Instance;
        initialPosition = transform.position;
        initialRotation = transform.eulerAngles;
        initialScale = transform.localScale;
    }

    public virtual void OnEnable()
    {
        APManager.OnGameOver += OnGameOver;
        APManager.OnGameReset += OnGameReset;
        APManager.OnGamePause += OnGamePause;
        APManager.OnPowerupsTaken += OnPowerupsTaken;
        APManager.OnGamestateChange += OnGamestateChange;
    }

    public virtual void OnDisable()
    {
        APManager.OnGameOver -= OnGameOver;
        APManager.OnGameReset -= OnGameReset;
        APManager.OnGamePause -= OnGamePause;
        APManager.OnPowerupsTaken -= OnPowerupsTaken;
        APManager.OnGamestateChange -= OnGamestateChange;
    }

    public virtual void OnGameStart()
    {

    }

    public virtual void OnGameReset()
    {
        transform.position = initialPosition;
        transform.eulerAngles = initialRotation;
        transform.localScale = initialScale;
    }

    public virtual void OnGamePause()
    {

    }

    public virtual void OnGameOver(GameplayData gameoverData)
    {

    }

    public virtual void OnGamestateChange(GameState gameState)
    {
        this.gameState = gameState;
    }

    public virtual void OnPowerupsTaken(Powerups powerups)
    {

    }
}
