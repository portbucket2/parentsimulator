﻿using UnityEngine;

namespace com.alphapotato.utility
{
    [DefaultExecutionOrder(ConstantManager.SDKSupportOrder)]
    public class SDKSupport : MonoBehaviour
    {
        public static SDKSupport Instance;

        public FBManager fbManager;
        public FBAnalyticsManager fbAnalyticsManager;

        #region Singleton Pattern
        private void Awake()
        {
            if (Instance != null)
            {

                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);
            fbManager.Initialize();
        }
        #endregion  Singleton Pattern
    }
}
