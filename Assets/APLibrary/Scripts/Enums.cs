﻿public enum LoadLevelType {

    LOAD_BY_NAME,
    LOAD_BY_INDEX
}

public enum GameState
{
    NONE,
    PAUSE,
    PLAYING,
    GAMEOVER,
    UI_SELECTION,
    BUSY
}

public enum Powerups
{
    NONE,
    SIZE_UP,
    MAGNET,
    SPEED_UP
}
