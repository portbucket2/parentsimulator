﻿public class GameplayData
{
    public bool isGameoverSuccess = false;
    public float gameStartTime = 0, gameStopTime = 0;
    public float totalTime = 0;
    public int gameScore = 0;
    public int levelNumber = -1;
}