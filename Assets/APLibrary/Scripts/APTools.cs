﻿/*
 * Developer E-mail: sandsoftimer@gmail.com
 * Facebook Account: https://www.facebook.com/md.imran.hossain.902
 * This is a manager which will give common supports. 
 * like, Scene FadeIn-Out Transition, Camera shaking, Slowmotioning, Object Pooling etc.
 *  
 */

using UnityEngine;

namespace com.alphapotato.utility
{
    [DefaultExecutionOrder(ConstantManager.APToolOrder)]
    public class APTools : MonoBehaviour
    {
        public static APTools Instance;

        public SceneManager sceneManager;
        public SavefileManager savefileManager;
        public CameraManager cameraManager;
        public PoolManager poolManager;

        #region Singleton Pattern
        private void Awake()
        {

            if (Instance != null)
            {

                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);
            //sceneManager.Initiallize();
        }
        #endregion  Singleton Pattern
    }
}