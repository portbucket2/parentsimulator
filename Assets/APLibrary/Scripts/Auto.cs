﻿using UnityEngine;

class MyClass
{
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void OnBeforeSceneLoadRuntimeMethod()
    {
        GameObject.Instantiate(Resources.Load("APTools") as GameObject).name = "APTools";
        GameObject.Instantiate(Resources.Load("SDKSupport") as GameObject).name = "SDKSupport";
    }

}