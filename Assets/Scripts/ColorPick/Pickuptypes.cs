﻿using System;
using UnityEngine;

public class Pickuptypes : APBehaviour
{
    public Rigidbody rig;
    public ColorType colorType;
    public bool pickable;
    
    //[HideInInspector]
    public Transform target;

    private void Start()
    {
        if (colorType.ToString().Equals("RED"))
        {
            ColorPickManager._instance.totalRedBall++;
        }
        else
        {
            ColorPickManager._instance.totalBlueBall++;
        }
    }

    float speed = 5;
    private void Update()
    {
        if (!gameState.Equals(GameState.PLAYING))
            return;

        if(target != null)
        {
            rig.velocity = speed * (target.position - transform.position);
        }
    }

    public void OnRelease()
    {
        target = null;
        rig.velocity = Vector3.zero;
        transform.parent = null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer.Equals(ConstantManager.GROUND_LAYER))
        {
            // Check if native ground
            if (colorType.ToString().Equals("RED") && other.tag.Equals(colorType.ToString()))
            {
                ColorPickManager._instance.satisfiedRedBall++;
                ColorPickManager._instance.OnCompleteATask();
            }
            else if (colorType.ToString().Equals("BLUE") && other.tag.Equals(colorType.ToString()))
            {
                ColorPickManager._instance.satisfiedBlueBall++;
                ColorPickManager._instance.OnCompleteATask();
            }

            pickable = true;

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer.Equals(ConstantManager.GROUND_LAYER))
        {
            // Check if native ground
            if (colorType.ToString().Equals("RED") && other.tag.Equals(colorType.ToString()))
            {
                ColorPickManager._instance.satisfiedRedBall--;
            }
            else if (colorType.ToString().Equals("BLUE") && other.tag.Equals(colorType.ToString()))
                ColorPickManager._instance.satisfiedBlueBall--;

            pickable = false;

        }
    }

    public void PickThis(Transform holder, ColorType colorType)
    {
        //Debug.Log(colorType);
        if (pickable && this.colorType.Equals(colorType))
        {
            transform.parent = holder;
            target = holder;
        }
    }
}
