﻿using DG.Tweening;
using TMPro;
using UnityEngine;

public class ColorPickManager : APManager
{
    public static ColorPickManager _instance;

    public TextMeshProUGUI redText, blueText;
    public Animator gameoverUIs, babyCryToLaugh;
    public GameObject initialText, bucket;

    public int totalRedBall, totalBlueBall;
    public int satisfiedRedBall, satisfiedBlueBall;
    public float gameStartTime = 2f;

    public override void Awake()
    {
        base.Awake();
        _instance = this;

        initialText.gameObject.SetActive(false);
        bucket.transform.DOLocalMove(new Vector3(-10, 0, 0), 0);
        bucket.SetActive(false);

        Invoke("GameStart", gameStartTime);
    }

    public override void GameStart()
    {
        bucket.SetActive(true);
        bucket.transform.DOLocalMove(new Vector3(0, 0, 0), 1).OnComplete(() => {

            initialText.SetActive(true);
            base.GameStart();
        });
    }

    private void Update()
    {
        redText.text = satisfiedRedBall + "/" + totalRedBall;
        blueText.text = satisfiedBlueBall + "/" + totalBlueBall;

        if (Input.GetMouseButtonDown(0))
            initialText.SetActive(false);
    }

    public override void OnCompleteATask()
    {
        if ((totalRedBall + totalBlueBall) == (satisfiedBlueBall + satisfiedRedBall))
        {
            gameplayData.isGameoverSuccess = true;
            gameplayData.gameStopTime = Time.time;

            GameOver();
        }
    }

    public override void GameOver()
    {
        base.GameOver();

        bucket.SetActive(false);
        babyCryToLaugh.SetInteger("Emoji", 100);
        gameoverUIs.SetBool("Hide", false);

    }

}
