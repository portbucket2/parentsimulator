﻿using DG.Tweening;
using UnityEngine;

public enum ColorType
{
    NONE,
    RED,
    BLUE
}

public class ColorPicker : APBehaviour
{
    public ColorType pickingAbiility;
    public GameObject[] baskets;
    public Vector3 offset;
    public Collider collider;
    public Transform holder;

    int layerMask;
    public override void Awake()
    {
        base.Awake();
        layerMask = LayerMask.GetMask("Ground");
        SetAbility(0);
    }

    public void SetAbility(int index)
    {
        if (!gameState.Equals(GameState.PLAYING))
            return;

        for (int i = 0; i < baskets.Length; i++)
        {
            baskets[i].SetActive(i == index);         
        }
        pickingAbiility = (ColorType)(index + 1);
    }

    private void Update()
    {
        if (!gameState.Equals(GameState.PLAYING))
            return;

        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition + offset);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {
                Vector3 pos = hit.point;
                pos.y = transform.position.y;

                transform.position = pos;
                collider.enabled = true;

            }
        }
        else
        {
            if (Input.GetMouseButtonUp(0))
            {
                collider.enabled = false;
                //foreach (Transform item in holder)
                //{
                //    item.GetComponent<Pickuptypes>().OnRelease();
                //}

                while(holder.childCount > 0)
                {
                    holder.GetChild(0).GetComponent<Pickuptypes>().OnRelease();
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer.Equals(ConstantManager.ENEMY_LAYER))
        {
            Pickuptypes pickuptypes = other.GetComponent<Pickuptypes>();
            pickuptypes.PickThis(holder, pickingAbiility);

        }
    }

}
