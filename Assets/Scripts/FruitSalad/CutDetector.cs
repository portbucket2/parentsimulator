﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CutDetector : APBehaviour
{
    public Slicer2D slicer2D;

    Rigidbody2D rig;
    Collider2D selfCollider;
    float timeToReach = 1f;
    public void Start()
    {
        slicer2D = GetComponent<Slicer2D>();
        slicer2D.AddResultEvent(SliceEvent);
        
        rig = GetComponent<Rigidbody2D>();
        selfCollider = GetComponent<Collider2D>();
    }

    private void SliceEvent(Slice2D slice2D)
    {
        FruitSaladManager.instance.OnTaskAdd(slice2D.gameObjects.Count - 1);
        foreach (GameObject item in slice2D.gameObjects)
        {
            item.layer = ConstantManager.PICKUPS_LAYER;

            if (!item.GetComponent<Rigidbody2D>())
                item.AddComponent<Rigidbody2D>();

            item.GetComponent<Rigidbody2D>().collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(ConstantManager.PLAYER_LAYER))
        {
            selfCollider.enabled = false;
            rig.isKinematic = true;
            rig.velocity = Vector2.zero;
            transform.DOScale(Vector3.zero, timeToReach);
            transform.DOMove(collision.transform.position, timeToReach).OnComplete(()=> {

                Destroy(gameObject);
            });

            FruitSaladManager.instance.OnCompleteATask();
        }
    }

    private void Update()
    {
        if(transform.position.y < -10)
        {
            Destroy(gameObject);
            FruitSaladManager.instance.OnCompleteATask();
        }

    }
}
