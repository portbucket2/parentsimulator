﻿using DG.Tweening;
using UnityEngine;

public class FruitSaladManager : APManager
{
    public static FruitSaladManager instance;

    public GameObject bowl, sliceController, initialText;
    public Animator gameoverUIs, babyCryToLaugh;
    public ParticleSystem bowlEffect;

    bool gameOverHappend = false;
    float gameStartTime = 3;

    public override void Awake()
    {
        base.Awake();
        instance = this;

        totalTaskToComplete = transform.childCount;
        currentCompletedTask = 0;

        sliceController.SetActive(false);
        initialText.gameObject.SetActive(false);
        Camera.main.transform.DOLocalMove(new Vector3(18, 1, -10), 0);
        
        Invoke("GameStart", gameStartTime);
    }

    public override void GameStart()
    {
        Camera.main.transform.DOLocalMove(new Vector3(0, 1, -10), 1).OnComplete(() => {

            initialText.gameObject.SetActive(false);
            sliceController.SetActive(true);
            initialText.SetActive(true);
            base.GameStart();
        });
    }

    public void OnTaskAdd(int count)
    {
        totalTaskToComplete += count;
    }

    public override void OnCompleteATask()
    {
        currentCompletedTask++;
        bowlEffect.Play();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            initialText.SetActive(false);

        if (transform.childCount == 0 && !gameOverHappend)
        {
            gameOverHappend = true;
            gameplayData.isGameoverSuccess = true;
            gameplayData.gameStopTime = Time.time;

            bowl.transform.DOLocalMove(new Vector3(18, 0, 0), 1.3f);
            Camera.main.transform.DOMove(new Vector3(18, 1, -10), 1).OnComplete(()=> {

                GameOver();

            });

        }
    }

    public override void GameOver()
    {
        base.GameOver();
        babyCryToLaugh.SetInteger("Emoji", 100);
        gameoverUIs.SetBool("Hide", false);
        Destroy(sliceController);
    }

}
