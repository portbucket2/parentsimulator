﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DetectorType
{
    NONE,
    TABLE,
    MILK
}

public class Detector : APBehaviour
{
    public DetectorType detector;

    private void OnCollisionEnter(Collision collision)
    {
        collision.collider.enabled = false;
        collision.transform.parent = transform;
        collision.collider.GetComponent<Rigidbody>().isKinematic = true;

        if (detector.Equals(DetectorType.MILK))
            PourCerealManager.Instance.OnCompleteATask();
        
    }
}
