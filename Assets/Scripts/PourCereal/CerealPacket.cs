﻿using TMPro;
using UnityEngine;

public class CerealPacket : APBehaviour
{
    public GameObject[] cerealPrefab;
    public float packetRotationAngle = 5f;
    public TextMeshProUGUI initialText;


    float initialZ;
    // Start is called before the first frame update
    void Start()
    {
        initialZ = transform.position.z;

        for (int i = 0; i < 50; i++)
        {
            APTools.poolManager.PrePopulateItem(cerealPrefab[Random.Range(0, cerealPrefab.Length)], 1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameState.Equals(GameState.PLAYING))
            return;

        if (Input.GetMouseButton(0))
        {
            initialText.gameObject.SetActive(false);

            Vector3 pos = APTools.cameraManager.GetWorldTouchPosition(Input.mousePosition);
            pos.z = initialZ + Mathf.Abs(pos.x) * packetRotationAngle;
            pos.y = transform.position.y;

            transform.position = pos;

            GameObject go = APTools.poolManager.Instantiate(cerealPrefab[Random.Range(0, cerealPrefab.Length)]);
            float xRange = 0.1f, zRange = 0.1f;
            pos.x = transform.position.x + Random.Range(-xRange, xRange);
            pos.y = transform.position.y;
            pos.z = transform.position.z + Random.Range(-zRange, zRange);
            go.transform.position = pos;
            go.GetComponent<Collider>().enabled = true;
            go.GetComponent<Rigidbody>().isKinematic = false;

            APTools.poolManager.Destroy(go, 1);

        }
    }
}
