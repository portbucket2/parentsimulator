﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PourCerealManager : APManager
{
    public Slider slider;
    public TextMeshProUGUI levelInfo, initialInfo;
    public Animator gameoverInfo;
    public Animator progressBarEmoji, babyCryToLaugh;

    public Transform crealPack;
    public float gameStartTime = 3;
    public override void Awake()
    {
        base.Awake();

        totalTaskToComplete = 500;
        currentCompletedTask = 0;
        levelInfo.text = "Level - " + APTools.sceneManager.GetLevelIndex() + 1;

        gameoverInfo.SetBool("Hide", true);
        initialInfo.gameObject.SetActive(false);
        crealPack.DOLocalMove(new Vector3(0, 10, -2.5f), 0);


        Invoke("GameStart", gameStartTime);
    }

    public override void GameStart()
    {
        crealPack.DOLocalMove(new Vector3(0, 2, -2.5f), 1).OnComplete(() => {

            initialInfo.gameObject.SetActive(true);
            base.GameStart();
        });
    }

    public override void OnCompleteATask()
    {
        base.OnCompleteATask();

        float value = Mathf.InverseLerp(0, totalTaskToComplete, currentCompletedTask);
        slider.value = value;
        progressBarEmoji.SetInteger("Progress", (int)(100 * value));
        babyCryToLaugh.SetInteger("Emoji", (int)(100 * value));

    }

    public override void GameOver()
    {
        base.GameOver();
        gameoverInfo.SetBool("Hide", false);
        crealPack.gameObject.SetActive(false);
    }

    public override void NextLevel()
    {
        base.NextLevel();
    }
}
