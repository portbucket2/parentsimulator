﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToyDetector : APBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer.Equals(ConstantManager.PICKUPS_LAYER))
            BabyToyManager.instance.OnCompleteATask();
    }
}
