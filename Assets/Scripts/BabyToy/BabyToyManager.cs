﻿using DG.Tweening;
using UnityEngine;

public class BabyToyManager : APManager
{
    public static BabyToyManager instance;
    public GameObject initalText;

    public float gameStartTime = 3f;
    public BabyMovement babyMovement;
    public BabyButton babyButton;

    public override void Awake()
    {
        base.Awake();
        instance = this;
        totalTaskToComplete = 8;
        currentCompletedTask = 0;

        initalText.gameObject.SetActive(false);
        babyMovement.enabled = false;
        babyButton.enabled = false;

        Invoke("GameStart", gameStartTime);
    }

    public override void GameStart()
    {
        babyMovement.enabled = true;
        babyMovement.transform.DORotate(new Vector3(0, 90, 0), 0.1f).OnComplete(()=> {

            initalText.gameObject.SetActive(true);
            babyButton.enabled = true;
            base.GameStart();
        });
    }

    public override void GameOver()
    {
        base.GameOver();
        babyMovement.transform.DORotate(new Vector3(0, 180, 0), 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            initalText.SetActive(false);
    }
}
