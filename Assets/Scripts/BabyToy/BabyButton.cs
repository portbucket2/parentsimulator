﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BabyButton : APBehaviour
{
    private void OnMouseDown()
    {
        if (!gameState.Equals(GameState.PLAYING))
            return;

        transform.DOMove(new Vector3(3.5f, transform.position.y, transform.position.z), 1);
    }
}
