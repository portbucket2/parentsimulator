﻿using DG.Tweening;
using UnityEngine;

public class BabyMovement : APBehaviour
{
    public Animator anim;
    public float speed;

    bool canMove = true;

    // Start is called before the first frame update
    void Start()
    {
        anim.SetTrigger("Start");
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameState.Equals(GameState.PLAYING) || !canMove)
            return;

        transform.position += transform.forward * speed * Time.deltaTime;
    }

    public override void OnGameOver(GameplayData gameoverData)
    {
        base.OnGameOver(gameoverData);

        anim.SetTrigger("Win");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer.Equals(ConstantManager.BOUNDARY_LAYER))
        {
            canMove = false;
            transform.DORotate(new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + 180, transform.eulerAngles.z), 0.5f).OnComplete(()=> {

                canMove = true;
            });
        }
    }
}
