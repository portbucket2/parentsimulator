﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CarDriveManager : APManager
{
    public static CarDriveManager instance;

    public TextMeshProUGUI levelText;
    public Animator levelInfoUIs, gameoverUIs;

    List<Vector3> points = new List<Vector3>();
    Vector3 lastPoints = Vector3.zero;

    public override void Awake()
    {
        base.Awake();
        instance = this;

        levelInfoUIs.SetBool("Hide", false);

        DOTween.Init();
        //levelText.text = "Level - " + APTools.sceneManager.GetLevelIndex() + 1;

        totalTaskToComplete = 1;
        currentCompletedTask = 0;

        GameStart();

    }

    public override void GameOver()
    {
        base.GameOver();

        levelInfoUIs.SetBool("Hide", true);
        gameoverUIs.SetBool("Hide", false);

    }

}
