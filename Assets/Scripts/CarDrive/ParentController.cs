﻿using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class ParentController : APBehaviour
{
    public float speed = 5f;
    public LineRenderer line;
    public float smoothLine = 0.1f;
    public bool startDrawing;
    public TextMeshProUGUI initialText;

    public Animator baby, parent;

    List<Vector3> points = new List<Vector3>();
    Vector3 lastPoints = Vector3.zero;
    bool isDone;

    public override void Awake()
    {
        base.Awake();
        isDone = false;
    }

    private void Update()
    {
        if (!gameState.Equals(GameState.PLAYING))
            return;

        if (isDone)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << ConstantManager.PLAYER_LAYER))
            {
                initialText.gameObject.SetActive(false);
                if (hit.collider.gameObject.layer.Equals(ConstantManager.PLAYER_LAYER))
                {
                    lastPoints = APTools.cameraManager.GetWorldTouchPosition(Input.mousePosition);
                    points = new List<Vector3>();
                    startDrawing = true;
                }
            }
        }

        if (Input.GetMouseButtonUp(0) && startDrawing)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << ConstantManager.GROUND_LAYER | 1 << ConstantManager.DESTINATION_LAYER))
            {
                if (!hit.collider.gameObject.layer.Equals(ConstantManager.DESTINATION_LAYER))
                    line.positionCount = 0;
                else
                {
                    isDone = true;

                    CarDriveManager.instance.gameplayData.gameStartTime = Time.time;
                    baby.SetTrigger("Walk");
                    parent.SetTrigger("Walk");
                    RunCar(0);
                }
            }

            startDrawing = false;
        }

        if (startDrawing)
        {
            Vector3 touchPoint = APTools.cameraManager.GetWorldTouchPosition(Input.mousePosition);
            if (Vector3.Distance(lastPoints, touchPoint) > smoothLine)
            {
                points.Add(touchPoint);
                lastPoints = touchPoint;

                line.positionCount++;
                touchPoint.y = 0.1f;
                line.SetPosition(line.positionCount - 1, touchPoint);
            }

        }
    }

    private void RunCar(int index)
    {

        float timeToComplete = Vector3.Distance(line.GetPosition(index), line.GetPosition(index + 1)) * Time.deltaTime / speed;

        //playerCar.LookAt(line.GetPosition(index));
        transform.DOLookAt(line.GetPosition(index + 1), timeToComplete * 5);
        //playerCar.transform.DORotate(line.GetPosition(index) - line.GetPosition(index + 1), timeToComplete);
        transform.transform.DOMove(line.GetPosition(index), timeToComplete).OnComplete(() => {

            if (index < line.positionCount - 2 && !endTravel)
                RunCar(++index);
            else
            {
                EndTravel();
            }
        }).SetEase(Ease.Linear);

    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.layer);
        if (other.gameObject.layer == ConstantManager.ENEMY_LAYER)
        {
            //CarDriveManager.instance.gameplayData.isGameoverSuccess = false;
            //CarDriveManager.instance.gameplayData.gameStopTime = Time.time;

            //CarDriveManager.instance.GameOver();
            //Destroy(gameObject);
        }
        else if (other.gameObject.layer == ConstantManager.DESTINATION_LAYER)
        {
            CarDriveManager.instance.gameplayData.isGameoverSuccess = false;
            CarDriveManager.instance.gameplayData.gameStopTime = Time.time;

            CarDriveManager.instance.GameOver();
            EndTravel();
        }
    }

    bool endTravel = false;
    void EndTravel()
    {
        endTravel = true;
        baby.SetTrigger("Idle");
        parent.SetTrigger("Idle");
        CarDriveManager.instance.OnCompleteATask();
    }
}
